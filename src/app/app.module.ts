import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { ClockComponent } from './clock/clock.component';

@NgModule({
  declarations: [
    AppComponent,
    ClockComponent,
  ],
  imports: [
    // Remark: because you haven't defined any routes, I have to pass an empty
    // route collection to forRoot, as the first parameter is mandatory.
    // this is required to have a provider for ActivatedRoute
    RouterModule.forRoot([]),
    BrowserModule,
    // ClockModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
