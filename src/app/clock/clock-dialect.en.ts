import { IClockDialect, ClockValue } from "./clock-dialect.type";

export class ClockDialectEn implements IClockDialect {

    // the grid is 11 x 11
    public GRID = [
        'I', 'T', 'L', 'I', 'S', 'A', 'S', 'A', 'M', 'P', 'M',
        'A', 'C', 'Q', 'U', 'A', 'R', 'T', 'E', 'R', 'D', 'C',
        'T', 'W', 'E', 'N', 'T', 'Y', 'F', 'I', 'V', 'E', 'X',
        'H', 'A', 'L', 'F', 'S', 'T', 'E', 'N', 'F', 'T', 'O',
        'P', 'A', 'S', 'T', 'E', 'R', 'U', 'N', 'I', 'N', 'E',
        'O', 'N', 'E', 'S', 'I', 'X', 'T', 'H', 'R', 'E', 'E',
        'V', 'O', 'U', 'R', 'F', 'I', 'V', 'E', 'T', 'W', 'O',
        'E', 'I', 'G', 'H', 'T', 'E', 'L', 'E', 'V', 'E', 'N',
        'S', 'E', 'V', 'E', 'N', 'T', 'W', 'E', 'L', 'F', 'E',
        'T', 'E', 'N', 'S', 'E', 'O', 'C', 'L', 'O', 'C', 'K',
        ' ', ' ', ' ', '*', '*', '*', '*', ' ', ' ', ' ', ' ',
      ];

    private setStateFunction = ( hour: number, minute: number): void  => {};

    mapTimeToGrid(
        hour: number,
        minute: number,
        setStateFunction: ( hour: number, minute: number) => void,
    ): void {
        this.setStateFunction = setStateFunction;
        let clockValues: ClockValue[] = this.mapClock(hour, minute);
        clockValues.forEach( value => this.setFields(value));
    }

    private setFields(clockValue: ClockValue) {

        const FIELD_MAP: Record<number, [number,number]> = {
          [ClockValue.zero]: [0, 0],
          [ClockValue.one]: [5 * 11 + 0, 3],
          [ClockValue.two]: [6 * 11 + 8, 3],
          [ClockValue.three]: [5 * 11 + 6, 5],
          [ClockValue.four]: [5 * 11 + 0, 4],
          [ClockValue.five]: [5 * 11 + 4, 4],
          [ClockValue.six]: [5 * 11 + 3, 3],
          [ClockValue.seven]: [7 * 11 + 0, 5],
          [ClockValue.eight]: [6 * 11 + 0, 5],
          [ClockValue.nine]: [4 * 11 + 7, 4],
          [ClockValue.ten]: [9 * 11 + 0, 3],
          [ClockValue.eleven]: [7 * 11 + 5, 6],
          [ClockValue.twelve]: [8 * 11 + 5, 6],
          [ClockValue.full_clock]: [9 * 11 + 5, 6],
          [ClockValue.half]: [3 * 11 + 0, 4],
          [ClockValue.five_m]: [2 * 11 + 6, 4],
          [ClockValue.ten_m]: [1 * 11 + 8, 3],
          [ClockValue.quarter]: [1 * 11 + 2, 6],
          [ClockValue.twenty]: [2 * 11 + 0, 6],
          [ClockValue.to]: [3 * 11 + 9, 2],
          [ClockValue.past]: [4 * 11 + 0, 4],
          [ClockValue.it]: [0 * 11 + 0, 2],
          [ClockValue.is]: [0 * 11 + 3, 2],
          [ClockValue.one_m]: [10 * 11 + 3, 1],
          [ClockValue.two_m]: [10 * 11 + 3, 2],
          [ClockValue.three_m]: [10 * 11 + 3, 3],
          [ClockValue.four_m]: [10 * 11 + 3, 4],
          [ClockValue.a]: [1 * 11 + 0, 1],
        };
        this.setStateFunction(FIELD_MAP[clockValue][0],FIELD_MAP[clockValue][1]);
    }

    private mapClock(hour: number, minute: number): ClockValue[] {
        let retArray : ClockValue[] = [];

        const modus_5 = minute % 5;
        minute = minute - modus_5;

        // handle the minutes below 5
        switch (modus_5) {
          case 1:
            retArray.push(ClockValue.one_m);
            break;
          case 2:
            retArray.push(ClockValue.two_m);
            break;
          case 3:
            retArray.push(ClockValue.three_m);
            break;
          case 4:
            retArray.push(ClockValue.four_m);
            break;
        }

        // handle the minutes
        switch (minute) {
          case 0:
            retArray.push(ClockValue.it);
            retArray.push(ClockValue.is);
            retArray.push(ClockValue.full_clock);
            break;
          case 5:
            retArray.push(ClockValue.five_m);
            retArray.push(ClockValue.past);
            break;
          case 10:
            retArray.push(ClockValue.ten_m);
            retArray.push(ClockValue.past);
            break;
          case 15:
            retArray.push(ClockValue.a);
            retArray.push(ClockValue.quarter);
            retArray.push(ClockValue.past);
            break;
          case 20:
            retArray.push(ClockValue.twenty);
            retArray.push(ClockValue.past);
            break;
          case 25:
            retArray.push(ClockValue.twenty);
            retArray.push(ClockValue.five_m);
            retArray.push(ClockValue.past);
            break;
          case 30:
            retArray.push(ClockValue.it);
            retArray.push(ClockValue.is);
            retArray.push(ClockValue.half);
            retArray.push(ClockValue.past);
            break;
          case 35:
            retArray.push(ClockValue.twenty);
            retArray.push(ClockValue.five_m);
            retArray.push(ClockValue.to);
            hour += 1;
            break;
          case 40:
            retArray.push(ClockValue.twenty);
            retArray.push(ClockValue.to);
            hour += 1;
            break;
          case 45:
            retArray.push(ClockValue.a);
            retArray.push(ClockValue.quarter);
            retArray.push(ClockValue.to);
            hour += 1;
            break;
          case 50:
            retArray.push(ClockValue.ten_m);
            retArray.push(ClockValue.to);
            hour += 1;
            break;
          case 55:
            retArray.push(ClockValue.five_m);
            retArray.push(ClockValue.to);
            hour += 1;
            break;
          }

        // handle the hour
        hour = hour % 12;
        if ( hour === 0 ) { hour = 12; }
        const value: ClockValue = hour as ClockValue;
        retArray.push(value);

        return retArray;
      }

}
