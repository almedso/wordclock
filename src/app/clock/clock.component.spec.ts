import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClockComponent } from './clock.component';
import { Location, LocationStrategy, } from '@angular/common';
import { SpyLocation, MockLocationStrategy } from '@angular/common/testing';


describe('ClockComponent', () => {
  let component: ClockComponent;
  let fixture: ComponentFixture<ClockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ClockComponent],
      providers: [
        { provide: Location, useClass: SpyLocation},
        { provide: LocationStrategy, useClass: MockLocationStrategy},
      ],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
